#!/bin/bash
resultdir=result-mte/hist
for entry in "$resultdir"/*; do

    filename="${entry##*/}"
    
    a=$(awk '{sum+=$2;} END{print sum;}' $entry)
    b=$(awk '{sum+=$2;} END{print sum;}' result-base-clang/hist/$filename)
    c=$((a - b))
    
    #echo $a
    #echo $b
    #echo $c

    #printf %.10f\\n "$((1000000000 * $c / $b))e-9"
    printf "%s," $filename
    printf "%.10f" "$((1000000000 * (($a - $b)) / $b))e-9"
    printf ",%d \n" $c
done