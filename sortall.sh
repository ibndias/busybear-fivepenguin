mkdir $1/sorted
for entry in "$1"/*
do
  filename="${entry##*/}"
  cat $entry | awk ' {print $2 " " $1} ' | sort > $1/sorted/$filename
  rm $entry
  mv $1/sorted/$filename $entry
done